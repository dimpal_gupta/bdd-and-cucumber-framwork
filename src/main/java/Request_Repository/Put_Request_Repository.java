package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Utility_Package_D.Excel_Data_Reader;

public class Put_Request_Repository extends Put_Endpoint{

	public static String put_TC_Request() throws IOException {
		
		ArrayList<String> excelData = Excel_Data_Reader.Read_Excel_data("API_Data.xlsx", "Put_API","Put_TC_2");
	//	System.out.println(excelData);
		String req_name= excelData.get(1);
		String req_job = excelData.get(2);
		
		String RequestBody = "{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";

		return RequestBody;
	}
}
