package Common_Utility_Package_D;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_Api_logs {
	// log handle :- create a directory
	public static File creat_log_directory(String Directory_name) {
		String current_project_directory = System.getProperty("user.dir");
		System.out.println("current directroy is " + current_project_directory);
		File Log_Directory = new File(current_project_directory + "\\Api_Logs\\" + Directory_name);
		deleteDirectory(Log_Directory);
		Log_Directory.mkdir();
		return Log_Directory;
		// System.out.println(Log_Directory);
	}

	public static boolean deleteDirectory(File directory) {
		boolean directorydeleted = directory.delete();
		if (directory.exists()) {
			File[] files = directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						deleteDirectory(file);
					} else {
						file.delete();
					}
				}
			}
			directorydeleted = directory.delete();
		} else {
			System.out.println("Directory does not exist.");
		}
		return directorydeleted;
	}

	public static void evidence_creator(File Directory_name, String file_name, String endpoint,String requestBody,
			String responseBody) throws IOException {
		// Step 1 :- Create file at given location
		File newfile = new File(Directory_name + "\\" + file_name + ".txt");
		System.out.println("new file created to save evidence : " + newfile.getName());

		// Step 2:- write data into the file
		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("endpont : " + endpoint + "\n\n"); // :- \n\n :- means go to next line, 2 time enter
		datawriter.write("Request Body : \n\n " + requestBody + "\n\n"); // write request body
		datawriter.write("Response Body : \n\n " + responseBody); // write response body
		datawriter.close();
		System.out.println("evidence is written in file : " + newfile.getName());
	}
}
