package Common_Utility_Package_D;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList; // ArrayList stored under java utility package
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Reader {

	public static ArrayList<String> Read_Excel_data(String File_name, String Sheet_name, String Test_Case_Name)
			throws IOException {

		ArrayList<String> ArrayData = new ArrayList<String>(); // ArrayList object created to use the method from
																// arraylist class
		// step 1 locate the file
		String Project_dir = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(Project_dir + "\\Input_Data\\" + File_name);// file location and file
																								// name
		// Step 2 Access(open) the located excel file
		XSSFWorkbook WB = new XSSFWorkbook(fis); // (WB)Work Book
		// Step 3 count the number of sheets available in the file
		int countofsheet = WB.getNumberOfSheets(); // for count sheets it is in WB
		System.out.println(countofsheet);

		// Step 4 Access the desired sheet
		for (int i = 0; i < countofsheet; i++) {
			String sheetname = WB.getSheetName(i);
			if (sheetname.equals(Sheet_name)) {
				System.out.println("inside the sheet : " + Sheet_name);
				XSSFSheet sheet = WB.getSheetAt(i);
				Iterator<Row> Rows = sheet.iterator();
				while (Rows.hasNext()) {
					Row currentRow = Rows.next();

					// step 5 Access the row corresponding desired test case

					if (currentRow.getCell(0).getStringCellValue().equals(Test_Case_Name)) {
						Iterator<Cell> Cell = currentRow.iterator();
						while (Cell.hasNext()) {
							String Data = Cell.next().getStringCellValue();
							// System.out.println(Data);
							ArrayData.add(Data);
						}
					}
				}
			}
		}
		WB.close();
		return ArrayData;
	}
}
