package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Utility_Package_D.Excel_Data_Reader;

public class Patch_Request_Repository extends Patch_Endpoint{

	public static String patch_tc1_request() throws IOException {
		
		ArrayList<String> excelData = Excel_Data_Reader.Read_Excel_data("API_Data.xlsx", "Patch_API", "Patch_TC_1");
		System.out.println(excelData);
		String req_name = excelData.get(1);
		String req_job = excelData.get(2);
		String RequestBody="{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		
		return RequestBody;
	}
}
