package Common_Method_Package_D;

import Request_Repository.Get_Endpoint;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;

import Common_Utility_Package_D.Excel_Data_Reader;

public class Trigger_get_API extends Get_Endpoint{
	
	
	public static int extractStatusCode(String URL) throws IOException {
		ArrayList<String> excelData = Excel_Data_Reader.Read_Excel_data("API_Data.xlsx", "Get_API", "Get_TC_1");
		System.out.println(excelData);
		String req_name = excelData.get(1);
		String req_job = excelData.get(2);
		
	int Status_code = given().when().get(URL)
			.then().extract().statusCode();
	
	return Status_code;
}
	public static String extractResponseBody(String URL) {
		String ResponseBody = given().when().get(URL)
				.then().extract().response().asString();
		
		return ResponseBody;
	}
}