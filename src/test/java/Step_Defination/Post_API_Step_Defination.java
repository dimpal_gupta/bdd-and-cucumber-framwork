package Step_Defination;

import java.io.IOException;
import java.time.LocalDateTime;

import Common_Method_Package_D.Trigger_API_Method;
import Request_Repository.Endpoint;
import Request_Repository.Post_Request_Repository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

import org.testng.Assert;

public class Post_API_Step_Defination {
	String req_body;
	String Post_Endpoint;
	int res_statusCode;
	String res_body;

	@Given("Enter name and job in Request body")
	public void enter_name_and_job_in_request_body() throws IOException {
		req_body = Post_Request_Repository.post_Tc1_Request();
		Post_Endpoint = Endpoint.post_endpoint();
		//throw new io.cucumber.java.PendingException();
	}

	@When("Send the Post Request with request body")
	public void send_the_post_request_with_request_body() {
		res_statusCode = Trigger_API_Method.extract_status_code(req_body, Post_Endpoint);
		res_body = Trigger_API_Method.extract_response_Body(req_body, Post_Endpoint);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Success response is received {int} as Status code")
	public void success_response_is_received_as_status_code(Integer int1) {
		Assert.assertEquals(res_statusCode, 201);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Response body as per Api Guide Document")
	public void response_body_as_per_api_guide_document() {
		JsonPath jsp_res = new JsonPath(res_body);
		String res_name = jsp_res.getString("name");
		//System.out.println(res_name);

		String res_job = jsp_res.getString("job");
		//System.out.println(res_job);
		String res_createdAt = jsp_res.getString("createdAt").substring(0, 10);

		// create an object of jsonpath to parse the requestBody
		JsonPath jsp_req = new JsonPath(req_body);
		String req_name = jsp_req.getString("name");
		//System.out.println("Request body name " + req_name);

		String req_job = jsp_req.getString("job");
	//	System.out.println("Request body job " + req_job);

		// date
		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 10);
		// System.out.println(ExpectedDate);

		// validate the responseBody
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, res_createdAt);
		//throw new io.cucumber.java.PendingException();
	}
}
